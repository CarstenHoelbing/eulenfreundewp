<?php
/*
 * der Code zum einrichten (im Adminbereich) des Themes
 * bassiert auf dem theme 'clean-simple-white' Code
 * 
*/

// WP nav menu
if ( function_exists( 'register_nav_menu' ) ) {
  register_nav_menu( 'primary', 'Menu welches im Footer angezeigt werden soll' );
 // register_nav_menu( 'secondary', 'Menu Header' );
}

// hier werden externe JavaScript Bibliotheken und dazugehoerige CSS Dateien geladen
function load_js_libs() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', get_bloginfo('template_url').'/js/jquery-1.6.4.min.js','1.6.4', false);
    wp_enqueue_script( 'jquery' );

    wp_enqueue_script('pretty', get_bloginfo('template_url').'/js/jquery.prettyPhoto.js', array('jquery'), '2.5.6', false);
    wp_enqueue_style('pretty', get_bloginfo('template_url').'/css/prettyPhoto.css', false, '2.5.6', 'all' );

 //  wp_enqueue_script('liquid-canvas', get_bloginfo('template_url').'/js/liquid-canvas.js', array('jquery'),'0.3', false);
 //  wp_enqueue_script('liquid-canvas-plugins', get_bloginfo('template_url').'/js/liquid-canvas-plugins.js', array('jquery'), '0.3', false);

    wp_enqueue_script('jquery.corner', get_bloginfo('template_url').'/js/jquery.corner.js', array('jquery'),'2.12', false);
}
//add_action('wp_enqueue_scripts', 'load_js_libs');


if ( function_exists('register_sidebar') )
register_sidebar();
//add_action( 'widgets_init', 'EulemFreunde_widgets_init' );

add_theme_support('automatic-feed-links');
?>