<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn 
 * die Startseite angezeigt werden soll.
 * 
*/

get_header();

get_sidebar();

?> 



<div id="content-bg">
    <div id="content">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
             <div class="news">

<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
         <h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
<h6><?php the_date('','',''); ?> um <?php the_time() ?> <?php edit_post_link(__('Edit This')); ?></h6>
</div><!-- end div class="post"-->
<p style="text-align:right;line-height: 1.5;"><?php the_content(__('(more...)')); ?><br></p>

        <div class="feedback">
                <?php wp_link_pages(); ?>
                <?php comments_popup_link(__('Kommentare (0)'), __('Kommentare (1)'), __('Kommentare (%)')); ?><br><br><br><br><br><br>
        </div>

<?php comments_template(); // Get wp-comments.php template ?>

</div><!-- end div class="news"-->

<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>

<?php endif; ?>



</div><!-- end div class="content"-->
</div><!-- end div class="content-bg"-->

<?php

get_footer(); 

?>