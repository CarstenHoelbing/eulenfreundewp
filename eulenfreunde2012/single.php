<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn eine 
 * Artikel angezeigt werden soll.
 * 
*/

get_header();

get_sidebar();

?> 


<div id="content-bg">
    <div id="content">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
             <div class="news">

<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
         <h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
<h6><?php the_date('','',''); ?> um <?php the_time() ?> <?php edit_post_link(__('Edit This')); ?></h6></div>
<p style="text-align:right;line-height: 1.5;"><?php the_content(__('(more...)')); ?><br></p>
<br><br><br><br><br><br>
        <div class="feedback">
                <?php wp_link_pages(); ?>
                <?php comments_popup_link(__('Kommentare (0)'), __('Kommentare (1)'), __('Kommentare (%)')); ?><br><br><br>
		<br><br><br>
        </div>

</div>

<?php comments_template(); // Get wp-comments.php template ?>

<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

</div>
</div>
</div>


<?php

get_footer(); 

?>