var Header = {
        addFade : function(selector){
                $("<span class=\"hover\"></span>").css("display", "none").prependTo($(selector));
                $(selector+" a").bind("mouseenter",function(){
                        $(selector+" .hover").fadeIn("slow");
                });
                $(selector+" a").bind("mouseleave",function(){
                        $(selector+" .hover").fadeOut("slow");
                });
        }
};

$(function(){
        Header.addFade("#fake_facebook");
        Header.addFade("#fake_twitter");
        Header.addFade("#fake_rss");
        Header.addFade("#fake_neues");
        Header.addFade("#fake_galerie");
        Header.addFade("#fake_eulenbutton"); 
});