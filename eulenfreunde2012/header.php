<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<title>
	<?php 
		if ( is_home() ) 
			echo bloginfo('name'); 
		else
		echo bloginfo('name') ."-" . the_title();
	?>
</title>

<meta name="description" content="Musik Theater Kultur Festival in Jena">
<meta name="keywords" lang="de" content="Musik, Theater, Kultur, Literatur, Festival, Jena, Campusradio, Eulenfreunde, Eule, eulenfreunde festival,eulenfreunde jena,eulen 2012, Peter Licht, like a stuntman, Fuck Art Let’s Dance, Fuck Art, Let’s Dance, eulenfreunde 2012,">
<meta name="keywords" lang="en-us" content="music, theater, culture, literature, festival, Jena, campusradio, Eulenfreunde, owl">
<meta name="keywords" lang="en" content="music, theater, culture, literature, festival, Jena, campusradio, Eulenfreunde, owl">

<META NAME="description" lang="de" CONTENT="Eulenfreunde 2012 das Campusradio Jena Festival. Das Musik, Theater, Kultur Festival in Jena. Vom 15. Januar 2012 bis zum 19. Januar 2012. ">
<META NAME="description" lang="en-us" CONTENT="Eulenfreunde 2012 the festival by Campusradio Jena.The festival with music, theater, literature and culture in Jena - Germany.">
<META NAME="description" lang="en" CONTENT="Eulenfreunde 2012 the festival by Campusradio Jena. The festival with music, theater, literature and culture in Jena - Germany.">

<META NAME="robots" CONTENT="index,follow">  
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta http-equiv="Content-Style-Type" content="text/css" />

<?php wp_head(); ?>

<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>"  media="screen" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/nivo-slider.css" type="text/css" media="screen" /> 
<link rel="shortcut icon" href="http://www.eulenfreun.de/favicon.png">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/fancybox/jquery.fancybox-1.3.4.css" media="screen" />


<?php wp_get_archives(array('type' => 'monthly', 'format' => 'link')); ?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/jquery.js"></script> <!-- jQuery -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/includes/hover.js"></script>  <!-- Hover JS -->

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/scripts/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/fancybox/video.js"></script>
<script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({directionNavHide:false});
    });

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-27426797-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	
</head>

<body>
<!-- Piwik -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://stats.campusradio-jena.de/" : "http://stats.campusradio-jena.de/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 4);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://stats.campusradio-jena.de/piwik.php?idsite=4" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Code -->


   <div id="header">
<p class="fastgross2"><a href="http://www.eulenfreun.de">15. - 19. Januar 2012</a><br></p>
<p class="gross"><a href="http://www.eulenfreun.de">eulenfreun.de</a><br></p>
<p class="fastgross"><a href="http://www.eulenfreun.de">besetzen die Stadt</a><br></p>
<a href="http://www.eulenfreun.de/tickets/"><img src="<?php bloginfo('template_directory'); ?>/images/tickets.png" alt="" border="0"></a>
<p style="text-align:right; margin-top:-14em; font-size:xx-small; padding-right:0;">
<a href="http://www.eulenfreun.de/wp-admin/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/navi-platz.png" alt="" width="80" height="56"></a>
<img src="<?php bloginfo('template_directory'); ?>/images/navi-platz.png" alt="" width="88" height="56">
<img src="<?php bloginfo('template_directory'); ?>/images/navi-platz.png" alt="" width="77" height="56">
<img src="<?php bloginfo('template_directory'); ?>/images/navi-platz.png" alt="" width="77" height="56">
<a href="http://www.eulenfreun.de/presse/"><img src="<?php bloginfo('template_directory'); ?>/images/presse.png" alt="" border="0"></a>
<a href="/kontakt">Kontakt</a> |
<a href="/Impressum">Impressum</a>
<div id="fake_twitter">
        <span class="hover" style="display:none"></span>
        <a href="http://twitter.com/campusradiojena" title="Twitter"></a>
</div>

<div id="fake_facebook">
        <span class="hover" style="display:none"></span>
        <a href="http://www.facebook.com/events/198415146905001/" title="Facebook"></a>
</div>

<div id="fake_google">
        <span class="hover" style="display:none"></span>
        <a href="#" title=""></a>
</div>

<img src="<?php bloginfo('template_directory'); ?>/images/navi-platz.png" alt="" width="22" height="56">
</p>
</div><!-- end div class="header"-->
<?php /*<img src="<?php bloginfo('template_directory'); ?>/images/footer-bg.png" alt="" border="0" width="280" height="90"> */?>
