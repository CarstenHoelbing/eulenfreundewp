	<div id="footer">

		<?php 
			// hier wird das menu aufgerufe
			wp_nav_menu( 
				array( 
					'theme_location' => 'primary',
					'menu_class' => 'menu',
					'items_wrap'      => '<ul id=\"%1$s\" class=\"%2$s\">%3$s</ul>'
				) 
			); 
		?>

		<img src="<?php bloginfo('template_directory'); ?>/images/footer-bg.png" alt="" />

	</div><!-- end div class="footer"-->


<?php wp_footer(); ?>

</body>
</html>