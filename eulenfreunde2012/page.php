<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn eine 
 * Seite/Page angezeigt werden soll.
 * 
*/

get_header();

get_sidebar();

?>

<div id="content-bg">
<div id="content">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="news">
		<?php the_content(); ?>
		<br><br>
<br>
<br>
<br>
<br>
<br>
	</div><!-- end div class="news"-->
<?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php edit_post_link('(Bearbeiten)'); ?>



</div><!-- end div class="content"-->
</div><!-- end div class="content-bg"-->


<?php 

get_footer(); 

?>