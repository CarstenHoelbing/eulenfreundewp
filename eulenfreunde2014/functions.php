<?php
/*
 * der Code zum einrichten (im Adminbereich) des Themes
 * bassiert auf dem theme 'clean-simple-white' Code
 * 
*/

	// WP nav menu
	add_action( 'init', 'register_my_menus' );
	
	function register_my_menus() {
		register_nav_menus(
			array(
				'main-menu' => 'Main Menu' 
			)
		);
	}
	
	add_theme_support('automatic-feed-links');

	// thumbnails einfügen
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size( 145, 150, true );
?>