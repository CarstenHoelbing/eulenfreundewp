<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn eine 
 * Seite/Page angezeigt werden soll.
 * 
*/

get_header();

wp_nav_menu( array( 'menu' => 'main-menu', 'container_class' => 'main_menu' ) );
?>
</div><!-- END  wrapper_header-->

<div id="wrapper"><!-- BEGIN wrapper-->
	
<div id="content_article"><!-- BEGIN content_article-->

<?php 
	while (have_posts()) : the_post(); 
?>

<div class="article"><!-- BEGIN article-->

<?php
	the_content();
?>
</div><!-- END article-->
        
<?php endwhile;?>
<br>
</div><!-- END  wrapper_header-->

<?php get_footer(); ?>