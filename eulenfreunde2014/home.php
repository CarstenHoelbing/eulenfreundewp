<?php
/**
 * Diese PHP-Seite wird auf der Startseite aufgefrufen
 * 
*/
?>

<!--[if IE 7]>
<html class="ie ie7" lang="de-DE">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="de-DE">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="de-DE">
<!--<![endif]-->
<head>

<title><?php 
	bloginfo( 'name' ); 
	wp_title();
	global $paged ;
	$page_max = $wp_query->max_num_pages;
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( 'Seite %s von %s ', $paged, $page_max );
?></title> 

<link rel="profile" href="http://gmpg.org/xfn/11">

<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

<meta name="description" content="Musik Theater Kultur Festival in Jena" />
<meta name="keywords" lang="de" content="Musik, Theater, Kultur, Literatur, Festival, Jena, Campusradio, Eulenfreunde, Eule, eulenfreunde festival,eulenfreunde jena,eulen 2014, eulenfreunde 2014,"/>
<meta name="keywords" lang="en-us" content="music, theater, culture, literature, festival, Jena, campusradio, Eulenfreunde, owl" />
<meta name="keywords" lang="en" content="music, theater, culture, literature, festival, Jena, campusradio, Eulenfreunde, owl" />

<meta name="description" lang="de" content="Eulenfreunde 2014 das Campusradio Jena Festival. Das Musik, Theater, Kultur Festival in Jena. 2014. " />
<meta name="description" lang="en-us" content="Eulenfreunde 2014 the festival by Campusradio Jena.The festival with music, theater, literature and culture in Jena - Germany." />
<meta name="description" lang="en" content="Eulenfreunde 2014 the festival by Campusradio Jena. The festival with music, theater, literature and culture in Jena - Germany." />


<meta name="robots" content="index,follow" />  

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta http-equiv="Content-Style-Type" content="text/css" />

	<?php
		remove_action('wp_head', 'wp_generator');  // Versionsnummer entfernen 
		wp_head();
	?>	

<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>"  media="screen" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />

<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" />

</head>

<body <?php body_class($class); ?> >
	
<div id="home_wrapper">

<div id="home_header">
	<div id="header_content">
	
		<div id="header_img_logo">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Logo-Eule-klein-100x116px.png" alt="Logo Campusradio Jena" width="100" height="116" />
		</div>
		<div id="header_info_text">
			<h1 class="header_text">Eulenfreunde Festival</h1>
			<h2 class="header_text">Wintersemester Session</h2>
			<h2 class="header_text">07. & 08.11.2014</h2>
		</div>
	</div>

<?php
	wp_nav_menu( array( 'menu' => 'main-menu', 'container_class' => 'main_menu' ) );
?>
</div>
	
<div id="home_spacer">
</div>

<?php get_footer(); ?>