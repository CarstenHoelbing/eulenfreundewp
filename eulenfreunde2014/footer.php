<?php
/**
 * Diese PHP-Seite wird immer am ende jeder Seite aufgerufen
 *
 * @package WordPress
 * @subpackage Eulenfreunde 2014
 * @since Eulenfreunde 2014 1.0
 */
?>	
<div id="footer-sponsoren">	
	<br/>
	Freunde und Förderer des Eulenfreunde Festivals:
	<br/>
	<br/>

	<a href="http://www.campusradio-jena.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer/campusmedium_campusradiojena.png" width="*" height="50" alt="Campusradio Jena"  title="Campusradio Jena" /></a>

	<br/>
	<a href="http://www.stw-thueringen.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer/stw_logo_transparent.png" width="*" height="30" alt="Studentenwerk Thüringen"  title="Studentenwerk Thüringen" /></a>
	<a href="http://www.kassablanca.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer/kassa.png" width="*" height="20" alt="Kassablanca"  title="Kassablanca"/></a>
	<a href="http://www.rosenkeller.org/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer/rose.png" width="*" height="20" alt="Rosenkeller"  title="Rosenkeller"/></a>
	<a href="http://www.justyo.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer/YO_logo_72.png" width="*" height="30" alt="YO!"  title="YO!"/></a>

<?php

/*
	<a href="http://studentsworx.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer/LOGO_studentsworx schwarz.png" width="*" height="30" alt="studentsworx" /></a>
	<a href="http://www.thalia.de//" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer/Thalia_Logo_transparent.png" width="*" height="30" alt="Thalia.de Online-Shop"  title="Thalia.de Online-Shop" /></a>		
 * 
 * 	<br/>
	<a href="http://www.stereoparkagency.com/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/footer/stereopark agency_transparent.png" width="*" height="30" alt="stereopark agency"  title="stereopark agency" /></a>	
	<a href="http://www.radio-okj.de/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer/radio_okj_transparent.png" width="*" height="20" alt="RADIO OKJ"  title="RADIO OKJ"></a>
 */?>
 
</div><!-- END footer-sponsoren -->
<br/>
<div id="footer-index">
	Copyright &copy; <?php echo date("Y") ?> <a href="<?php home_url(); ?>"><?php bloginfo('name'); ?></a>. All rights reserved.
	<?php echo get_template() ?> Theme by <a href="http://www.hoelbing.net/">Carsten Hoelbing</a>.<br/>
</div>

<div id="footer">
	Redaktion Campusradio Jena -- Ernst-Abbe-Fachhochschule Jena -- Carl-Zeiss-Promenade 2 -- 07745 Jena -- Tel. +49 3641 205-796  <br/>			
	<a href="<?php bloginfo('rss2_url'); ?>" title="RSS">RSS</a>  |
	<a href="<?php bloginfo('url'); ?>/impressum/" title="Impressum">Impressum</a> |
	<a href="<?php bloginfo('url'); ?>/datenschutz/" title="Datenschutz">Datenschutz</a> | 
	<a href="<?php echo wp_login_url(); ?>" title="Login">Login</a>
</div>
	
<div id="sidebar_socialLinks">
	<a href="https://www.facebook.com/eulenfreundefestival/" target="_blank">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fb_icon.small.png" alt="hier geht es zum Facebook Auftritt des Eulenfreunde Festival" title="hier geht es zum Facebook Auftritt des Eulenfreunde Festival"  width="50" height="50"/>
	</a>
</div><!-- end sidebar_socialLinks -->

</div ><!-- END wrapper-->

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(["trackPageView"]);
	_paq.push(["enableLinkTracking"]);

	(function() {
		var u = (("https:" == document.location.protocol) ? "https" : "http") + "://stats.stura.uni-jena.de/";
		_paq.push(["setTrackerUrl", u + "piwik.php"]);
		_paq.push(["setSiteId", "12"]);
		var d = document, g = d.createElement("script"), s = d.getElementsByTagName("script")[0];
		g.type = "text/javascript";
		g.defer = true;
		g.async = true;
		g.src = u + "piwik.js";
		s.parentNode.insertBefore(g, s);
	})();
</script>
<!-- End Piwik Code -->

<?php wp_footer(); ?>

</body>
</html>