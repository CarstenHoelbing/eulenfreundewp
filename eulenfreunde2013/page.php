<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn eine 
 * Seite/Page angezeigt werden soll.
 * 
*/
?>

<?php get_header(); ?>

<div id="content_box"> <!-- BEGIN content_box -->

<?php
	global $more;
	$more = 0;

while (have_posts()) : the_post(); ?>

	<div class="article"><!-- BEGIN article-->
			<?php
				the_content();
				echo "<br/>";
				echo '<div class="tags-post">';
				//the_tags('<Tags: ', ", ", '');
				//echo "<br/><br/>";
				//echo the_time('j F, Y'); echo " - "; the_time('H:i');echo " Uhr";
				//echo "<br/>";
				edit_post_link('<br/><br/>Beitrag bearbeiten.',' ','');
				echo '</div>';
			?>
	</div><!-- END article-->
        
<?php endwhile;?>

<?php get_sidebar();?>

</div> <!-- END content_box -->

<?php get_footer(); ?>