<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" > 
<head>

<title><?php wp_title(); ?> <?php bloginfo( 'name' ); ?></title> 

<meta name="description" content="Musik Theater Kultur Festival in Jena" />
<meta name="keywords" lang="de" content="Musik, Theater, Kultur, Literatur, Festival, Jena, Campusradio, Eulenfreunde, Eule, eulenfreunde festival,eulenfreunde jena,eulen 2013, eulenfreunde 2013,"/>
<meta name="keywords" lang="en-us" content="music, theater, culture, literature, festival, Jena, campusradio, Eulenfreunde, owl" />
<meta name="keywords" lang="en" content="music, theater, culture, literature, festival, Jena, campusradio, Eulenfreunde, owl" />

<?php
/**/
?>
<meta name="description" lang="de" content="Eulenfreunde 2013 das Campusradio Jena Festival. Das Musik, Theater, Kultur Festival in Jena. 2013. " />
<meta name="description" lang="en-us" content="Eulenfreunde 2013 the festival by Campusradio Jena.The festival with music, theater, literature and culture in Jena - Germany." />
<meta name="description" lang="en" content="Eulenfreunde 2013 the festival by Campusradio Jena. The festival with music, theater, literature and culture in Jena - Germany." />

<meta name="robots" content="index,follow" />  
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta http-equiv="Content-Style-Type" content="text/css" />

	<?php
		remove_action('wp_head', 'wp_generator');  // Versionsnummer entfernen 
		wp_head();
	?>	

<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>"  media="screen" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />

<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.png" />

</head>

<body>

<div id="wrapper">
<div id="body_header"><a href="<?php bloginfo('url'); ?>"> </a> </div>

<div id="menus">
	<?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>
</div>