<?php
/**
 * Diese PHP-Seite wird auf der Startseite und folgend aufgefrufen
 * 
*/
?>

<?php get_header();?>

<div id="content_box"> <!-- BEGIN content_box -->

<?php

	$page = (get_query_var('page')) ? get_query_var('page') : 1;
	
	// ausschliessen die Kategorie 'allgemein' wird angezeigt 
	query_posts(array ( 'category_name' => 'allgemein','paged' => get_query_var( 'paged' )));
	global $more;
	$more = 0;
	
	while (have_posts()) : the_post(); 
	
		get_template_part( 'content_index', 'index' );
	
	endwhile;

get_sidebar();?>

<div id="page-nav">
	<div id="older">
		<?php next_posts_link('Nächste Artikel &raquo;'); ?>
	</div>
	<div id="newer">
		<?php previous_posts_link('&laquo; Vorherige Artikel'); ?>
	</div>
</div>

</div> <!-- END content_box -->



<?php get_footer(); ?>