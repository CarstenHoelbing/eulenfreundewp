<?php
/**
 * Diese PHP-Seite wird immer am ende jeder Seite aufgerufen
 * 
 * @package WordPress
 * @subpackage Eulenfreunde 2013
 * @since Eulenfreunde 2013 1.0
*/
?>	
	<div id="footer-sponsoren">	
		<br/>
		Freunde und Förderer des Eulenfreunde Festivals:
		<br/>
		<br/>
		<a href="http://stura.inside-fhjena.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/stura_eafh_logo_transparent.png" width="*" height="50" alt="Studierendenrat der EAH Jena" /></a>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		<a href="https://stura.uni-jena.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/stura.uni-jena.logo.png" width="*" height="50" alt="Studierendenrat der FSU Jena" /></a>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		<a href="http://www.stw-thueringen.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/stw_logo_transparent.png" width="*" height="50" alt="Studentenwerk Thüringen" /></a>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		<a href="http://www.radio-okj.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/radio_okj_transparent.png" width="*" height="50" alt="RADIO OKJ" /></a>
		<br/>

		<a href="http://www.rosenkeller.org/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/rose.png" width="*" height="50" alt="Rosenkeller" /></a>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		<a href="http://www.kassablanca.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/kassa.png" width="*" height="36" alt="Kassablanca" /></a>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		<a href="http://www.the-ater-cafe.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/theatercafe.png" width="*" height="50" alt="TheaterCafe" /></a>
		<br/>

		<a href="http://www.tanzundklangkombinat.de/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/tanz-klangkombinat-logo.png" width="*" height="50" alt="Tanz und Klangkombinat" /></a>

	</div>

	<div id="footer-index">
		Copyright &copy; <?php echo date("Y") ?> <b><a href="<?php home_url(); ?>"><?php bloginfo('name'); ?></a>. All rights reserved.<br/>
		<?php echo get_template() ?> Theme by <a href="http://www.hoelbing.net/">Carsten Hoelbing</a></b>.<br/>
	</div>
	
	<div id="footer">
		Redaktion Campusradio Jena -- Ernst-Abbe-Fachhochschule Jena -- Carl-Zeiss-Promenade 2 -- 07745 Jena<br/>
		Tel. +49 3641 205-796 -- E-Mail: redaktion@campusradio-jena.de <br/>			
		<a href="<?php bloginfo('rss2_url'); ?>" title="RSS">RSS</a>  |
		<a href="<?php bloginfo('url'); ?>/impressum/" title="Impressum">Impressum</a> | 
		<a href="<?php bloginfo('url'); ?>/sender/kontakt/" title="Kontakt">Kontakt</a> | 
		<a href="<?php echo wp_login_url(); ?>" title="Login">Login</a>
	</div>
	
<br/>

</div><!-- END wrapper -->

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["trackPageView"]);
  _paq.push(["enableLinkTracking"]);

  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://stats.stura.uni-jena.de/";
    _paq.push(["setTrackerUrl", u+"piwik.php"]);
    _paq.push(["setSiteId", "12"]);
    var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
    g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->

<?php wp_footer(); ?>

</body>
</html>