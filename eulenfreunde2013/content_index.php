<?php
/**
 * Content anzeige
 * wird auf der Startseite und folgend aufgefrufen
 *
 * @package WordPress
 * @subpackage CampusRadioJenaWPTemplate
 * @since CampusRadioJenaWPTemplate 1.0
 */

?>
<div class="article"><!-- START article -->

	<?php 
		if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) 
		{
			echo '<div class="article_img">';
			the_post_thumbnail();
			echo '</div>';
		}
	?>
	
	<div class="article_ueberschrift">
		 <a href="<?php the_permalink() ?>"><?php trim(the_title())?></a>
	</div>
	
	<?php 	 
		the_content('<br/><br/>Weiterlesen ... »');
		//the_content('Weiterlesen ...');
		echo "<br/>";
		echo '<div class="tags-post">';
		the_tags('Tags: ', ", ", '');
		echo "<br/><br/>";
		echo the_time('j F, Y'); echo " - "; the_time('H:i');echo " Uhr";
		echo "<br/>";
		edit_post_link('Beitrag bearbeiten.<br/><br/>',' ','');
		echo '</div>';
		comments_popup_link('Keine Kommentare bisher', '1 Kommentar bisher', '% Kommentare bisher ', 'Kommentar-Link', 'Kommentare ausgeschaltet');
	?>

</div><!-- END article -->