<?php
/**
 * Diese PHP-Seite wird aufgefrufen wenn eine 
 * Artikel angezeigt werden soll.
 * 
*/
?>

<?php get_header(); ?>

<div id="content_box"> <!-- BEGIN content_box -->

<?php
	global $more;
	$more = 0;

while (have_posts()) : the_post(); ?>
<div class="article"><!-- BEGIN article-->

<?php 
	if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) 
	{
		echo '<div class="article_img">';
		the_post_thumbnail();
		echo '</div>';
	}
?>
	<div class="article_ueberschrift">
		 <a href="<?php the_permalink() ?>"><?php trim(the_title())?></a>
	</div>
			<?php 

			the_content('<br/><br/>Weiterlesen ... »');
			//the_content('Weiterlesen ...');
			echo "<br/>";
			echo '<div class="tags-post">';
			the_tags('<Tags: ', ", ", '');
			echo "<br/><br/>";
			echo the_time('j F, Y'); echo " - "; the_time('H:i');echo " Uhr";
			echo "<br/>";
			edit_post_link('Beitrag bearbeiten.<br/><br/>',' ','');
			echo '</div>';
			//comments_popup_link('Keine Kommentare bisher', '1 Kommentar bisher', '% Kommentare bisher ', 'Kommentar-Link', 'Kommentare ausgeschaltet');
			?>
		<br/>

<?php comments_template(); ?>

	</div><!-- END article-->
	
<?php endwhile;?>

<?php get_sidebar();?>

</div> <!-- END content_box -->

<?php get_footer(); ?>