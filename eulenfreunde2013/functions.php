<?php
/*
 * der Code zum einrichten (im Adminbereich) des Themes
 * bassiert auf dem theme 'clean-simple-white' Code
 * 
*/

// WP nav menu
add_action( 'init', 'register_my_menus' );

function register_my_menus() {
	register_nav_menus(
		array(
			'primary-menu' => 'header Menu' ,
			'secondary-menu' => 'Sidebar Menu' 
		)
	);
}

register_sidebar(
	array(
		'name' => 'sidebar_banner',
		'id' => 'sidebar_banner',
		'description' => 'Diese Sidebar ist nur fuer die Events Banner gedacht',
		'before_title' => '',
		'after_title' => '',
		'before_widget' => '',
		'after_widget' => '',
	)
);

register_sidebar(
	array(
		'name' => 'sidebar_banner_comments',
		'id' => 'sidebar_banner_comments',
		'description' => 'Diese Sidebar ist das Widget Kommentare anzeigen gedacht',
		'before_title' => '',
		'after_title' => '',
		'before_widget' => '',
		'after_widget' => '',
	)
);

// thumbnails einfügen
add_theme_support('post-thumbnails');
set_post_thumbnail_size( 145, 150, true );

// hier werden externe JavaScript Bibliotheken und dazugehoerige CSS Dateien geladen
function load_js_libs() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', get_bloginfo('template_url').'/js/jquery-1.6.4.min.js','1.6.4', false);
    wp_enqueue_script( 'jquery' );

    wp_enqueue_script('pretty', get_bloginfo('template_url').'/js/jquery.prettyPhoto.js', array('jquery'), '2.5.6', false);
    wp_enqueue_style('pretty', get_bloginfo('template_url').'/css/prettyPhoto.css', false, '2.5.6', 'all' );

 //  wp_enqueue_script('liquid-canvas', get_bloginfo('template_url').'/js/liquid-canvas.js', array('jquery'),'0.3', false);
 //  wp_enqueue_script('liquid-canvas-plugins', get_bloginfo('template_url').'/js/liquid-canvas-plugins.js', array('jquery'), '0.3', false);

    wp_enqueue_script('jquery.corner', get_bloginfo('template_url').'/js/jquery.corner.js', array('jquery'),'2.12', false);
}
//add_action('wp_enqueue_scripts', 'load_js_libs');


if ( function_exists('register_sidebar') )
register_sidebar();
//add_action( 'widgets_init', 'EulemFreunde_widgets_init' );

add_theme_support('automatic-feed-links');
?>